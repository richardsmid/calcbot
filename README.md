
# This is the official CalcBot repository.
### *This bot is developed by Richard Smid and Ondrej Matysek.*

The bot currently only supports basic operations, but we are working on support for longer, more complicated operations and graphical ways to display them.

In the future, we want the bot to be easily deployed as a docker container with docker-compose.
